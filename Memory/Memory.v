module insmemory(address,out);
input[63:0] address;
output[64;0] out;
integer i;
reg[n-1;0] ram [0:255];

initial 
	begin
		for(i = 0; i < 256; i = i + 1)
			ram[i] = i;  
	end

begin
  ram[0] = 64'b00110011;
  ram[1] = 64'b01000111;
  ram[2]= 64'b00000011;
  ram[3]= 64'b01100011;
end

endmodule;
