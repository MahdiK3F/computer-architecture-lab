
`timescale 1ns / 1ns
module testBench  ; 
 
  reg    d   ; 
  reg    clk   ; 
  wire  [3:0]  Cout   ; 
  wire  [3:0]  S   ; 
  pca  
   DUT  ( 
       .d (d ) ,
      .clk (clk ) ,
      .Cout (Cout ) ,
      .S (S ) ); 



// "Repeater Pattern" Repeat Forever
// Start Time = 0 ns, End Time = 10 us, Period = 200 ns
  initial
  begin
   repeat(25)
   begin
	    d  = 1'b0  ;
	   #200   d  = 1'b1  ;
	   #200 ;
// 10 us, repeat pattern in loop.
   end
  end


// "Clock Pattern" : dutyCycle = 50
// Start Time = 0 ns, End Time = 10 us, Period = 100 ns
  initial
  begin
	  clk  = 1'b0  ;
	 # 50 ;
// 50 ns, single loop till start period.
   repeat(99)
   begin
	   clk  = 1'b1  ;
	  #50  clk  = 1'b0  ;
	  #50 ;
// 9950 ns, repeat pattern in loop.
   end
	  clk  = 1'b1  ;
	 # 50 ;
// dumped values till 10 us
  end

  initial
	#20000 $stop;
endmodule
