module FullAdder4Bit(A,B,Cin,S,Cout);

input[3:0] A,B;
input [3:0]Cin;
output [3:0] S;
output [3:0] Cout;

fullAdder FA_0(A[0],B[0],Cin[0],S[0],Cout[0]);
fullAdder FA_1(A[1],B[1],Cin[1],S[1],Cout[1]);
fullAdder FA_2(A[2],B[2],Cin[2],S[2],Cout[2]);
fullAdder FA_3(A[3],B[3],Cin[3],S[3],Cout[3]);

endmodule
